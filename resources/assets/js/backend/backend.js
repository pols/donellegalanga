'use strict';

// JavaScript dependencies
import '../bootstrap';
import axios from 'axios';
import VueRouter from 'vue-router';
import router from './routes'
import VeeValidate from 'vee-validate';
import Vuex from 'vuex'
import VueSimplemde from 'vue-simplemde'
import VueCropper from 'vue-cropperjs';

require('vue-router');

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VeeValidate);
Vue.use(VueSimplemde);

Vue.component('vue-pagination', require('./components/Pagination.vue'));

let store = new Vuex.Store(require('./store'))

window.Vue = require('vue');
window.jquery = require('jquery');
window.axios = require('axios');


// Make sure to inject the router with the router option to make the
// whole app router-aware.
const vm = new Vue({
  router,
  store,
  computed: {
			// getToonBg: function() {
		//     return 'toon-bg-' + this.admin.id % 6 ;
		//   }
  },
  methods: {

  },
  created(){

  },
  components: {
  		sidebar: require('./components/sidebar.vue'),
      search: require('./components/search.vue'),
  }
}).$mount('#pols-app');