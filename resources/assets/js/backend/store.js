
module.exports = {
	state: {	
		blogs: {},
        pagination: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        imgSrc: '',
        fileName: '',
	},
	mutations: {
		renderBlog(state, data){
			state.blogs = data.data;
			state.pagination = data;
		},
		saveImgSrc(state, data){
			state.imgSrc = data.imgSrc;
			state.fileName = data.fileName;
		},
		
	},
	actions: {
		populateBlog(context,page){
			var url = '/admin/populateblog?page='+page
            axios.get(url)
              .then( response => {
                context.commit('renderBlog',response.data);
              })
			
		},
		trigSaveImgSrc(context,data){
			context.commit('saveImgSrc',data);
		}
	},
	getters: {
		blogs: state => state.blogs,
		pagination: state => state.pagination,
		imgSrc: state => state.imgSrc,
		fileName: state => state.fileName,
	}
}