import VueRouter from 'vue-router';

const Blog = require('./views/blog.vue')
const Test2 = require('./components/dashboard/test2.vue')
const Test3 = require('./components/dashboard/test3.vue')


const routes = [
  { path: '/', component: Blog},
  { path: '/blog', component: Blog},
  { path: '/test2', component: Test2},
  { path: '/test3', component: Test3},
]

export default new VueRouter({
	routes,
	linkActiveClass: 'active'
})