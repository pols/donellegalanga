
export default {
	methods: {
		isImageType: function(filename) {
                const ext = filename.substr(filename.lastIndexOf('.') + 1)
                switch (ext.toLowerCase()) {
                    case 'jpg':
                    case 'jpeg':
                    case 'gif':
                    case 'bmp':
                    case 'png':
                        //etc
                        return true;
                    default:
                }
                return false;
        },
	}
}