<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RevAgainColumnImagesBlogTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blogs', function ($table) {
            $table->dropColumn('image_id');
        });

        Schema::table('images', function (Blueprint $table) {
            $table->integer('blog_id')->after('id');
            $table->enum('portion', ['featured', 'album','avatar'])->after('blog_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
