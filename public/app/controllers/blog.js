app.controller('blogController', function($scope, $http, API_URL) {
	//retrieve blog listing from API
	
    $http.get(API_URL + "admin/bloginit")
        .then(function successCallback(response){
            $scope.blogs = response.data;
        }); 

    //$scope.form_title = "Add Blog";
    $scope.status = ["draft", "publish"];

     $scope.confirmDelete = function(id){
     	 var isConfirmDelete = confirm('Are you sure you want this record?');
     	 if( isConfirmDelete ){
     	 	$http({
     	 		method: 'DELETE',
     	 		url: API_URL + 'admin/blog/' + id
     	 	}).then(function successCallback(response) {
                console.log(response);
                location.reload();
              }, function errorCallback(response) {
                alert(response);
              });
     	 }
     }


     //show modal form
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Blog";
                $scope.blog = {};
                break;
            case 'edit':
                $scope.form_title = "Blog Detail";
                $scope.id = id;
                
                $http.get(API_URL + 'admin/show/' + id)
                    .then(function successCallback(response){
                        //console.log(response);
                        $scope.blog = response.data;
                    }); 
                break;
            default:
                break;
        }
        console.log(id);
        $('#myModal').modal('show');
    }

    $scope.save = function(modalstate,id){
        var url = API_URL + 'admin/blog'
        if( modalstate === 'edit' ){
            url += "/" + id;
        }
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.blog),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response){
            console.log(response);
            location.reload();
        });
    }
});