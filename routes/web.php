<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Models\Admin;


Route::get('/', function () {
    return view('frontend.blog.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


// Admin

Route::get('admin/login', 'AdminController@login');


Route::get('admin/attempt', 'AdminController@attempt');
Route::post('admin/attempt', 'AdminController@attempt');



Route::group(array('before' => 'admin', 'prefix' => 'admin', 'middleware' => 'backend'), function() {

	// BLOG
	Route::get('bloginit','AdminBlogController@bloginit');

	Route::group(array('prefix'=>'blog'),function(){
		
		Route::post('/','AdminBlogController@store');
		Route::get('showalltags','AdminBlogController@populateTags');

		Route::group(array('prefix'=>'{id}'),function(){
			Route::get('/','AdminBlogController@single');
			Route::post('/','AdminBlogController@update');
		});

	});


	
	Route::get('blog','AdminBlogController@index');
	Route::get('populateblog', 'AdminBlogController@populateBlog');
	Route::post('blog','AdminBlogController@store');
	//Route::get('blog/{id}','AdminBlogController@single');
	//Route::post('blog/{id}','AdminBlogController@update');
	//Route::get('show/{id}','AdminBlogController@show');

	Route::delete('blog/{id}','AdminBlogController@destroy');
	//Route::delete('blog','AdminBlogController@index');
	//Route::resource('blog' , 'AdminBlogController', array('as'=>'blog') );

	Route::get('logout', 'AdminController@logout');
});


//for Frontend
Route::get('blog','BlogController@index');