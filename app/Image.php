<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = array('id','name','created_at','ext_name');

    /*public function blog(){
    	return $this->belongsTo('\App\Blog');
    }*/
}
