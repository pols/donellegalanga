<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = array('id','title');

    public function tags(){
    	return $this->belongsToMany('\App\Tag');
    }

    
    public function featuredImage(){
    	return $this->hasOne('\App\Image');
    } 
}
