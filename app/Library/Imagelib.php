<?php
namespace App\Library;
 
class Imagelib {

   public $allowed_types = [
	    'image/bmp',
	    'image/png',
	    'image/jpg',
	    'image/gif',
	    'image/jpeg'
   ];
 
   public function encode($blogID,$filename) {
        return md5($filename.$blogID);
   }

   /**
     * Return file extension by mime type
     *
     * @param string $type
     * @return boolean|string
     */
    public function getFileExtension($type){
        if (!$this->isAllowedType($type)) {
            return FALSE;
        }
        $arr = explode('/', $type);
        return $arr[1];
    }

    /**
     * Checks if the mime type is allowed
     *
     * @param string $type
     * @return boolean
     */
    public function isAllowedType($type){
        return in_array($type, $this->allowed_types);
    }
 
}