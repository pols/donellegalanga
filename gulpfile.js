const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir(mix => {
    mix.sass('resources/assets/scss/frontend/mixins/_animation.scss',)
}); 
/*
elixir(function(mix) {
    mix.styles([
        'backend/bootstrap/dist/css/bootstrap.min.css',
        'backend/nprogress/nprogress.css',
        'backend/fontawesome/css/font-awesome.min.css',
        'backend/animate.css/animate.min.css',
        'build/css/custom.min.css'
    ])
    .scripts([
        'app/lib/angular/angular/angular.min.js',
    	'backend/jquery/dist/jquery.min.js',
    	'backend/bootstrap/dist/js/bootstrap.js',
        'backend/fastclick/lib/fastclick.js',
        'backend/nprogress/nprogress.js',
        'build/js/custom.min.js',
    ])
    .copy('resources/assets/css/backend/fontawesome/fonts', 'public/fonts')
    .copy('resources/assets/vendor', 'resources/assets/js/app/lib/angular')
});
END OLD CODE
*/

// FOR FRONT END I USED GULP

//var gulp = require('gulp');
// Requires the gulp-sass plugin
/*
var sass = require('gulp-sass');

gulp.task('frontsass', function() {
  gulp.src('resources/assets/scss/frontend/mixins/_animation.scss') // Gets the styles.scss file
    .pipe(sass()) // Passes it through a gulp-sass task
    .pipe(gulp.dest('public/css2/')) // Outputs it in the css folder
});
*/
